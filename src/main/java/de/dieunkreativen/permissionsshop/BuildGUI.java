package de.dieunkreativen.permissionsshop;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class BuildGUI {
	FileConfiguration cfg = PermissionsShop.getSelf().getConfig();
	
	public BuildGUI(PShopTrait pShopTrait) {
		// TODO Auto-generated constructor stub
	}

	public void openGUI(Player p) {
	    Inventory inv = Bukkit.createInventory(null, 9, ConfigManager.getShopName());
	    double moneyPlayer = PermissionsShop.econ.getBalance(p);
	    
	    for(int i=0; i<=9; i++)
	    {
	    	if (cfg.getString("InventorySlots." + i) != null) {
	    		ItemStack item = new ItemStack(Material.getMaterial(cfg.getString("InventorySlots." + i + ".Item")));
	    		ItemMeta meta = item.getItemMeta();
	    		ArrayList<String> lore = new ArrayList<String>();
	    		
	    		if (p.hasPermission(cfg.getString("InventorySlots." + i + ".SetPermission"))) {
	    			item = new ItemStack(Material.BARRIER);
	    			meta.setDisplayName(ChatColor.RED + cfg.getString("InventorySlots." + i + ".DisplayName"));
	    		}
	    		else {
	    			meta.setDisplayName(ChatColor.GREEN + cfg.getString("InventorySlots." + i + ".DisplayName"));
	    		}
	    		
	    		lore.add(ChatColor.translateAlternateColorCodes('&', cfg.getString("InventorySlots." + i + ".Lore")));
	    		
	    		if (moneyPlayer >= cfg.getInt("InventorySlots." + i + ".Price")) {
	    			lore.add(ChatColor.GREEN + "Preis: " + cfg.getString("InventorySlots." + i + ".Price"));
	    		}
	    		else {
	    			lore.add(ChatColor.RED + "Preis: " + cfg.getString("InventorySlots." + i + ".Price"));
	    		}
	    		
	    		
	    		meta.setLore(lore);
	    		item.setItemMeta(meta);
	    		inv.setItem(i, item); 
	    	}
	    } 
	    p.openInventory(inv);
	}
}

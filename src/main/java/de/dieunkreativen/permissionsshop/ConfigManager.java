package de.dieunkreativen.permissionsshop;

import org.bukkit.configuration.file.FileConfiguration;

public class ConfigManager {
	static FileConfiguration cfg;
	
	
	
	public static void loadConfig() {
		cfg = PermissionsShop.getSelf().getConfig();
	}
	
	public static String getShopName () {
		return cfg.get("ShopName").toString();
	}
}

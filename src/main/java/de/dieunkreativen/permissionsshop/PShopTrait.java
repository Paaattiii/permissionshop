package de.dieunkreativen.permissionsshop;

import net.citizensnpcs.api.event.NPCRightClickEvent;
import net.citizensnpcs.api.persistence.Persist;
import net.citizensnpcs.api.trait.Trait;
import net.citizensnpcs.api.util.DataKey;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;


public class PShopTrait extends Trait {
	 
	public PShopTrait() {
		super("PShop");
		plugin = (PermissionsShop) Bukkit.getServer().getPluginManager().getPlugin("PermissionsShop");
	}
 
	PermissionsShop plugin = null;
	private final BuildGUI gui = new BuildGUI(this);
 
	boolean SomeSetting = false;
 
        // see the 'Persistence API' section
        @Persist("mysettingname") boolean automaticallyPersistedSetting = false;
 
	// Here you should load up any values you have previously saved (optional). 
        // This does NOT get called when applying the trait for the first time, only loading onto an existing npc at server start.
        // This is called AFTER onAttach so you can load defaults in onAttach and they will be overridden here.
        // This is called BEFORE onSpawn, npc.getBukkitEntity() will return null.
	public void load(DataKey key) {
		SomeSetting = key.getBoolean("SomeSetting", false);
	}
 
	// Save settings for this NPC (optional). These values will be persisted to the Citizens saves file
	public void save(DataKey key) {
		key.setBoolean("SomeSetting",SomeSetting);
	}
	
	@EventHandler
	public void npcRightClickEvent(NPCRightClickEvent e) {
		//check trait
		if (!e.getNPC().hasTrait(PShopTrait.class)) return;
		
		//check permission
		if (!e.getClicker().hasPermission("PermissionsShop.use")) {
			e.getClicker().sendMessage(ChatColor.RED + "Keine Berechtigung");
			return;
		}
		gui.openGUI(e.getClicker());
	}
 
	//Run code when your trait is attached to a NPC. 
        //This is called BEFORE onSpawn, so npc.getBukkitEntity() will return null
        //This would be a good place to load configurable defaults for new NPCs.
	@Override
	public void onAttach() {
		plugin.getServer().getLogger().info(npc.getName() + "has been assigned PermissionsShop!");
	}
 
        // Run code when the NPC is despawned. This is called before the entity actually despawns so npc.getBukkitEntity() is still valid.
	@Override
	public void onDespawn() {
        }
 
	//Run code when the NPC is spawned. Note that npc.getBukkitEntity() will be null until this method is called.
        //This is called AFTER onAttach and AFTER Load when the server is started.
	@Override
	public void onSpawn() {
 
	}
 
        //run code when the NPC is removed. Use this to tear down any repeating tasks.
	@Override
	public void onRemove() {
        }
 
}
package de.dieunkreativen.permissionsshop;

import java.util.logging.Level;
import java.util.logging.Logger;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class PermissionsShop extends JavaPlugin {
	Logger log;
	PluginDescriptionFile pdfFile = this.getDescription();
	public static Economy econ = null;
	public static Permission perms = null;
	public static PermissionsShop plugin;
	private final InventoryListener listener = new InventoryListener(this);
	
    @Override
    public void onLoad() {
            log = getLogger();
    }
    
	@Override
	public void onEnable() {
		plugin = this;
		saveDefaultConfig();
		ConfigManager.loadConfig();
		setupEconomy();
		setupPermissions();
		setupCitizens();
		PluginManager pm = this.getServer().getPluginManager();
		pm.registerEvents(listener, this);
		log.info(pdfFile.getName() + " version " + pdfFile.getVersion() + " is enabled!");
	}
	
	@Override
	public void onDisable() {
		log.info(pdfFile.getName() + " version " + pdfFile.getVersion() + " is disabled!");
	}
	
    @Override
    public void reloadConfig() {
    	super.reloadConfig();
    }
	
    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }
    
    private boolean setupPermissions() {
        RegisteredServiceProvider<Permission> rsp = getServer().getServicesManager().getRegistration(Permission.class);
        perms = rsp.getProvider();
        return perms != null;
    }
    
    private void setupCitizens() {
    	if(getServer().getPluginManager().getPlugin("Citizens") == null || getServer().getPluginManager().getPlugin("Citizens").isEnabled() == false) {
    		getLogger().log(Level.SEVERE, "Citizens 2.0 not found or not enabled");
    		getServer().getPluginManager().disablePlugin(this);	
    		return;
    	}	

    	//Register your trait with Citizens.        
    	net.citizensnpcs.api.CitizensAPI.getTraitFactory().registerTrait(net.citizensnpcs.api.trait.TraitInfo.create(PShopTrait.class).withName("PShop"));	
}
    
    public static PermissionsShop getSelf() {
		return plugin;
	}
    
	@Override
	public boolean onCommand(final CommandSender sender, Command cmd, String label, String[] args) {	
		if (cmd.getName().equalsIgnoreCase("pshop")) {
			if(args.length > 0) {
				if(args[0].equalsIgnoreCase("reload") && sender.hasPermission("PermissionsShop.admin")) {
					reloadConfig();
				}
			}
			return true;
		}
		return false;
	}
}

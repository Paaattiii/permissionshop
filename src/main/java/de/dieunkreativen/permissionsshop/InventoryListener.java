package de.dieunkreativen.permissionsshop;

import net.milkbowl.vault.economy.EconomyResponse;

import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class InventoryListener implements Listener {
	public PermissionsShop plugin;
	
	public InventoryListener(PermissionsShop instance) {
		plugin = instance;
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		Inventory inventory = event.getInventory(); 
		if (inventory.getName().equals("Permission Shop")) {
			event.setCancelled(true); 
			Player p = (Player) event.getWhoClicked();
			int clickedSlot = event.getSlot();
			ItemStack clicked = event.getCurrentItem();
					
			if(clicked.getType() != Material.AIR) {
				String permission = PermissionsShop.getSelf().getConfig().getString("InventorySlots." + clickedSlot + ".SetPermission");
				String requiredPermission = PermissionsShop.getSelf().getConfig().getString("InventorySlots." + clickedSlot + ".RequiredPermission");
				String requiredMessage = PermissionsShop.getSelf().getConfig().getString("InventorySlots." + clickedSlot + ".RequiredMessage");
				int price = PermissionsShop.getSelf().getConfig().getInt("InventorySlots." + clickedSlot + ".Price");
				
				if (price == 0 || permission == null) {
					return;
				}
				
				if (p.hasPermission(permission)) {
					p.sendMessage(ChatColor.RED + "Das hast du bereits gekauft!");
					return;
				}
				
				if (requiredPermission != null && !p.hasPermission(requiredPermission)) {
					if (requiredMessage != null) {
						p.sendMessage(requiredMessage);
						return;
					}
				}
				
				if (PermissionsShop.econ.getBalance(p) >= price) {
					EconomyResponse response = PermissionsShop.econ.withdrawPlayer(p, price);
					  if (response.transactionSuccess()) {
						  PermissionsShop.perms.playerAdd(null, p, permission);
						  
						  p.sendMessage(ChatColor.GREEN + "Erfolgreich gekauft");
						  p.closeInventory();
						  Effect eff = Effect.valueOf("LAVA_POP");
						  p.getWorld().playEffect(p.getLocation(), eff, 0);
						  p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1F, 0);
					  }
				}
				else {
					p.sendMessage(ChatColor.RED + "Du hast nicht genug Geld!");
				}
			}
		}
	}

}
